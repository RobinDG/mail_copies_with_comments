# Documentation pour l'envoi des copies aux étudiants par mail

Cette documentation est là pour vous aider à utiliser le script `mail_copies_with_comments.py` (qui fait exactement ce que son nom laisse entendre).

Il s'agit d'un script compagnon des examens corrigés/encodés automatiquement avec [AMC](https://bitbucket.org/RobinDG/templates-amc/wiki/Home) pour les cours de physique générale de Marc Haelterman à l'ULB. Autrement dit, ce script ne fonctionne que pour des examens ayant été générés et corrigés avec AMC.

Il est bon de savoir qu'AMC a déjà une fonctionnalité d'envoi des copies aux étudiants par mail, intégrée à son interface utilisateur. Néanmoins, ce script dispose d'une fonction supplémentaire : lors de la correction des questions ouvertes des examens de physique, les correcteurs peuvent cocher des cases nommées de A à G correspondant chacune à un commentaire défini par le correcteur. En allant lire dans la base de données AMC quelles cases ont été cochées par le correcteur pour chaque étudiant, et sur base de la liste de tous les contenue dans le fichier `comments.xml` ainsi que de la liste des mails des étudiants, le script `mail_copies_with_comments.py` envoie automatiquement à chaque étudiant sa copie accompagnée des commentaires des correcteurs pour chacune des questions.

Le script peut également générer des statistiques simples permettant de savoir combien de fois chaque commentaire a été fait. Cela peut donner une idée des difficultés les plus souvent rencontrées par les étudiants.

Ce script a été créé par l'ancien assistant [Robert Vanden Eynde](https://robertvandeneynde.be/#fr) et a ensuite reçu quelques modifications par le (plus ancien encore) assistant [Robin De Gernier](https://robindg.pythonanywhere.com/contact/).

## Utilisation en ligne de commandes

### Prérequis

Il vous faudra évidemment une installation Python (version >= 3.6) avec tous les packages nécessaires, càd les packages indiqués dans le fichier `requirements.txt`.

Si vous ne disposez pas des packages en question, vous pouvez les installer un à un ou utiliser la commande suivante pour les installer sans effort :

```
pip install -r requirements.txt
```

### Etapes pour l'utilisation en ligne de commandes

Pour utiliser le script `mail_copies_with_comments.py` en ligne de commandes vous devez :

- copier-coller le script dans le dossier de l'examen pour lequel vous voulez envoyer les copies, **à côté** du dossier du projet AMC (normalement appelé `generateurAMC_A)`
- modifier les options directement dans le script
    - ouvrez le script dans un éditeur de texte de qualité (ex : [Atom](www.atom.io))
    - modifiez les valeurs des variables en MAJUSCULES qui se trouvent au début du script (les commentaires vous aideront à comprendre le rôle et l'utilisation de chaque variable).
- faire des tests avant de lancer l'envoi final aux étudiants (voir sous-section suivante)
- la commande pour lancer le script (que ce soit pour les tests ou pour l'envoi final) est la suivante :

```
python3 mail_copies_with_comments.py
```

**Remarque pour Windows**

Si vous êtes sur Windows et que vous avez plusieurs installations parallèles de Python (Python 2.X, Python 3.X, divers environnements Python...), vous pouvez lancer la commande avec la version de votre choix en adaptant la commande suivante (donnée pour la version Python 3.6) :

```
py -3.6 mail_copies_with_comments.py
```




### Tests pré-envoi

Quand vous aurez réglé tous les paramètres, avant d'envoyer les mails aux étudiants, faites les tests suivants :
1. *Lancer le script sans envoyer les mails* pour vérifier qu'il trouve le projet, la liste d'étudiants, etc. Pour cela, la configuration doit être légèrement modifiée pour avoir :
    - `SEND_MAIL = False` (ne pas envoyer de mails)
    - `TO_ONE_PERSON = 'rdegerni@ulb.ac.be'` où vous pouvez remplacer l'adresse mail par la vôtre (n'envoyer les mails qu'à une adresse, pas nécessaire avec le réglage précédent, mais c'est une sécurité en plus)
    - `MAX_EMAIL = None` (générer les mails pour tous les étudiants afin de vérifier que la liste d'étudiants fonctionne bien - les mails ne seront pas envoyés évidemment)
2. *Lancer le script en envoyant les 10 premiers mails à une seule adresse (la vôtre)* pour vérifier que les mails que vous recevez contiennent la bonne copie pour le bon étudiant, qu'il n'y a pas de typo ou de problème d'affichage dans le message ou dans les commentaires des correcteurs (si vous utilisez le fichier `comments.xml`). Pour cela, la configuration doit être modifée de sorte à avoir :
    - `SEND_MAIL = True` (envoyer les mails)
    - `TO_ONE_PERSON = 'rdegerni@ulb.ac.be'` où vous pouvez remplacer l'adresse mail par la vôtre (n'envoyer les mails qu'à une adresse)
    - `MAX_EMAIL = 10` (n'envoyer les mails que pour les 10 premiers étudiants)

Si les résultats de ces deux tests vous satisfont, vous pouvez remodifier les trois paramètres afin d'envoyer les mails aux étudiants :
- `SEND_MAIL = True` (envoyer les mails)
- `TO_ONE_PERSON = None`
- `MAX_EMAIL = None` (envoyer tous les mails)


## Syntaxe du fichier comments.xml

Si les correcteurs de votre examen AMC ont utilisé le système de commentaires pour les questions ouvertes (cf. intro de cette documentation), vous pouvez retranscrire leurs commentaires (qu'ils vous auront idéalement fournis par mail - et pas de manière manuscrite - ou, encore mieux, via un formulaire Google Form que vous aurez créé pour l'occasion) dans le fichier comments.xml en respectant une syntaxe assez simple.

Plutôt que de décrire cette syntaxe, voici un exemple de fichier `comments.xml` valide avec quelques explications directement intégrées en commentaires. Vous verrez, c'est assez simple.

```xml
    <questions>
        <question id="1"> <!-- default matcher="simple" -->
            <!-- order of <comment> is important -->
            <comment name="a"> <!-- markdown/latex/html convention by default: paragraph on blank line. (syntax="rawmd") -->
                La vitesse finale n'est pas de zéro.
                En effet, on a de l'énergie.

                Ce qui nous amène à une conclusion.
            </comment>
            <comment name="B"> <!-- case is not important, B upper case = b lower case -->
                Attention à la perte d'énergie.
            </comment>
        </question>
        <question id="2">
            <comment name="A">
                L'accélération n'est pas une force.
            </comment>
        </question>
    </questions>
```

Pour créer votre propre fichier de commentaires, copiez-collez simplement cet exemple dans le fichier `comments.xml` vide et complétez-le.

## Problèmes de formats

Il peut arriver que certains caractères spéciaux (les accents essentiellement) dans la liste des étudiants ou dans le fichier `comments.xml` posent les problèmes suivants :

- caractères qui ne s'affichent pas correctement dans les mails
- noms d'étudiants non reconnus

Ces problèmes sont bien souvent liés à un mauvais format d'encodage des fichiers incriminés. L'encodage recommandé est normalement "UTF-8", mais dans certains cas il faut essayer d'autres encodages comme le format "Windows 1252".

Pour changer le format d'encodage d'un fichier, il faut utiliser un éditeur de texte avec des fonctionnalités avancées, comme [PSPad](https://www.pspad.com/fr/).
